const Product = require("../models/productModel");
const ErrorHandler = require("../utils/errorHander");
const catchAsyncErrors = require("../middlewares/catchAsyncErrors");
const ApiFeatures = require("../utils/apifeatures");
// Create Product -- Admin
exports.createProduct = catchAsyncErrors(async (req, res, next) => {
    const product = await Product.create(req.body);
    res.status(200).json({
        success:true,
        product
    });
});

// Get All Products
exports.getAllProducts = catchAsyncErrors(async (req, res) => {
    const apiFeature = new ApiFeatures(Product.find(), req.query).search().filter();
    const products = await apiFeature.query;
    res.status(200).json({
        success:true,
        products
    });
});

// Get Product Deatils
exports.getProductDetails = catchAsyncErrors(async (req, res, next) => {
    const product = await Product.findById(req.params.id);

    if(!product){
        return next(new ErrorHandler("Product not found", 404));
    };

    res.status(200).json({
        success:true,
        product
    }) ;
});

//Update Product -- Admin
exports.updateProduct = catchAsyncErrors(async (req, res) => {
    let product = Product.findById(req.params.id);

    if(!product){
        return res.status(500).json({
            success:false,
            message:"Product not found"
        });
    };
    product = await Product.findByIdAndUpdate(req.params.id, req.body,{
        new:true,
        runValidators: true,
        useFindAndModify:false
    });

    res.status(200).json({
        success:true,
        product
    });
});

// Delete Product
exports.deleteProduct = catchAsyncErrors(async(req,res,next) => {
    const product = await Product.findById(req.params.id);

    if(!product){
        return res.status(500).json({
            success:false,
            message: "Product not found"
        });
    };

    await product.remove();

    res.status(200).json({
        success:true,
        message:"Product Deleted Successfully"
    });

});
